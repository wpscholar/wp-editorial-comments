<?php

/**
 * Class WP_Editorial_Comments_AJAX
 */
class WP_Editorial_Comments_AJAX {

	public static function initialize() {
		foreach ( array( 'add', 'save', 'delete', 'delete-all' ) as $action ) {
			add_action(
				'wp_ajax_' . WP_Editorial_Comment::POST_TYPE . '-' . $action,
				array( __CLASS__, str_replace( '-', '_', $action ) )
			);
		}
	}

	/**
	 * Add a new editorial comment
	 */
	public static function add() {
		try {

			$action = WP_Editorial_Comment::POST_TYPE . '-add';

			if ( ! wp_verify_nonce( filter_input( INPUT_POST, 'nonce' ), $action ) ) {
				throw new Exception( 'Invalid nonce' );
			}

			$post_id = filter_input( INPUT_POST, 'id', FILTER_VALIDATE_INT );
			$post = get_post( $post_id );

			if ( ! $post_id || ! $post ) {
				throw new Exception( 'Invalid post id' );
			}

			if ( ! current_user_can( 'edit_posts', $post_id ) ) {
				throw new Exception( 'Permission denied' );
			}

			$comment_id = WP_Editorial_Comment::add( $post, filter_input( INPUT_POST, 'comment' ) );
			$comment = get_post( $comment_id );
			setup_postdata( $comment );

			ob_start();
			require WP_EDITORIAL_COMMENTS_DIR . 'templates/comment.php';
			$html = ob_get_clean();

			wp_send_json_success( array(
				'html' => trim( preg_replace( '/(\t|\n)/S', '', $html ) )
			) );

		} catch ( Exception $e ) {
			wp_send_json_error( array( 'message' => $e->getMessage() ) );
		}
	}

	/**
	 * Update an editorial comment
	 */
	public static function save() {
		try {

			$action = WP_Editorial_Comment::POST_TYPE . '-save';

			if ( ! wp_verify_nonce( filter_input( INPUT_POST, 'nonce' ), $action ) ) {
				throw new Exception( 'Invalid nonce' );
			}

			$post_id = filter_input( INPUT_POST, 'id', FILTER_VALIDATE_INT );
			$post = get_post( $post_id );

			if ( ! $post_id || ! $post ) {
				throw new Exception( 'Invalid post id' );
			}

			if ( WP_Editorial_Comment::POST_TYPE !== $post->post_type ) {
				throw new Exception( 'Invalid post type' );
			}

			$post_parent_id = $post->post_parent;
			$post_parent = get_post( $post_parent_id );

			if ( ! $post_parent_id || ! $post_parent ) {
				throw new Exception( 'Invalid post parent' );
			}

			if ( get_current_user_id() !== absint( $post_parent->post_author ) && ! current_user_can( 'moderate_comments' ) ) {
				throw new Exception( 'Permission denied' );
			}

			if ( get_current_user_id() === absint( $post_parent->post_author ) && ! current_user_can( 'edit_posts', $post_parent_id ) ) {
				throw new Exception( 'Permission denied' );
			}

			$comment_id = WP_Editorial_Comment::edit( $post_id, filter_input( INPUT_POST, 'comment' ) );
			$comment = get_post( $comment_id );

			$content = apply_filters( 'the_content', $comment->post_content );
			$content = str_replace( ']]>', ']]&gt;', $content );

			wp_send_json_success( array( 'comment' => $content ) );

		} catch ( Exception $e ) {
			wp_send_json_error( array( 'message' => $e->getMessage() ) );
		}
	}

	/**
	 * Delete an editorial comment
	 */
	public static function delete() {
		try {

			$action = WP_Editorial_Comment::POST_TYPE . '-delete';

			if ( ! wp_verify_nonce( filter_input( INPUT_POST, 'nonce' ), $action ) ) {
				throw new Exception( 'Invalid nonce' );
			}

			$post_id = filter_input( INPUT_POST, 'id', FILTER_VALIDATE_INT );
			$post = get_post( $post_id );

			if ( ! $post_id || ! $post ) {
				throw new Exception( 'Invalid post id' );
			}

			if ( WP_Editorial_Comment::POST_TYPE !== $post->post_type ) {
				throw new Exception( 'Invalid post type' );
			}

			if ( ! current_user_can( 'moderate_comments' ) ) {
				throw new Exception( 'Permission denied' );
			}

			WP_Editorial_Comment::delete( $post_id );

			wp_send_json_success();

		} catch ( Exception $e ) {
			wp_send_json_error( array( 'message' => $e->getMessage() ) );
		}
	}

	/**
	 * Delete all editorial comments
	 */
	public static function delete_all() {
		try {

			$action = WP_Editorial_Comment::POST_TYPE . '-delete-all';

			if ( ! wp_verify_nonce( filter_input( INPUT_POST, 'nonce' ), $action ) ) {
				throw new Exception( 'Invalid nonce' );
			}

			$post_id = filter_input( INPUT_POST, 'id', FILTER_VALIDATE_INT );
			$post = get_post( $post_id );

			if ( ! $post_id || ! $post ) {
				throw new Exception( 'Invalid post id' );
			}

			if ( ! current_user_can( 'moderate_comments' ) ) {
				throw new Exception( 'Permission denied' );
			}

			WP_Editorial_Comment::deleteAll( $post );

			wp_send_json_success();

		} catch ( Exception $e ) {
			wp_send_json_error( array( 'message' => $e->getMessage() ) );
		}
	}

}