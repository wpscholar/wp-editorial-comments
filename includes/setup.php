<?php

/**
 * Class WP_Editorial_Comments
 */
class WP_Editorial_Comments {

	/**
	 * Setup plugin functionality
	 */
	public static function initialize() {

		self::register_post_type();
		add_action( 'add_meta_boxes', array( __CLASS__, 'add_meta_box' ), 10, 2 );
		add_action( 'save_post', array( __CLASS__, 'on_save' ), 10, 2 );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'admin_enqueue_scripts' ) );

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			WP_Editorial_Comments_AJAX::initialize();
		}

	}

	/**
	 * Enqueue admin styles and scripts.
	 */
	public static function admin_enqueue_scripts() {
		wp_enqueue_style( WP_Editorial_Comment::POST_TYPE, WP_EDITORIAL_COMMENTS_URL . '/assets/wp-editorial-comments.css' );
		wp_enqueue_script( WP_Editorial_Comment::POST_TYPE, WP_EDITORIAL_COMMENTS_URL . '/assets/wp-editorial-comments.js', array( 'jquery' ) );
	}

	/**
	 * Register post type
	 */
	public static function register_post_type() {
		$args = array(
			'public'   => false,
			'supports' => array( 'editor' ),
		);
		$args = apply_filters( WP_Editorial_Comment::POST_TYPE . '-post_type_args', $args );
		register_post_type( WP_Editorial_Comment::POST_TYPE, $args );
	}

	/**
	 * Check if post type is supported for this feature.
	 *
	 * @param string $post_type
	 *
	 * @return bool
	 */
	public static function is_post_type_supported( $post_type ) {
		if ( WP_Editorial_Comment::POST_TYPE === $post_type ) {
			return false;
		}
		$is_supported = post_type_supports( $post_type, 'editor' ) || post_type_supports( $post_type, 'wp-editorial-comments' );

		return apply_filters( 'wp_editorial_comments-post_type_support', $is_supported );
	}

	/**
	 * Add meta box
	 *
	 * @param string $post_type
	 * @param WP_Post $post
	 */
	public static function add_meta_box( $post_type, $post ) {
		if ( is_object( $post ) && is_a( $post, 'WP_Post' ) && 'auto-draft' !== $post->post_status && self::is_post_type_supported( $post_type ) && current_user_can( 'edit_post', $post->ID ) ) {
			add_meta_box(
				'wp-editorial-comments',
				__( 'Editorial Comments', 'wp-editorial-comments' ),
				array( __CLASS__, 'render_meta_box' )
			);
		}
	}

	/**
	 * Render meta box
	 */
	public static function render_meta_box() {
		require __DIR__ . '/../templates/comment-list.php';
		require __DIR__ . '/../templates/comment-form.php';
	}

	/**
	 * Add comment on save
	 *
	 * @param int $post_id
	 * @param WP_Post $post
	 */
	public static function on_save( $post_id, WP_Post $post ) {
		if ( 'auto-draft' !== $post->post_status && current_user_can( 'edit_post', $post_id ) ) {
			$comment = filter_input( INPUT_POST, WP_Editorial_Comment::POST_TYPE );
			if ( ! empty( $comment ) ) {
				remove_action( 'save_post', array( __CLASS__, 'on_save' ), 10 );
				WP_Editorial_Comment::add( $post, $comment );
				add_action( 'save_post', array( __CLASS__, 'on_save' ), 10, 5 );
			}
		}
	}

}