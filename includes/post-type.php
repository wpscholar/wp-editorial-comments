<?php

/**
 * Class WP_Editorial_Comment
 */
class WP_Editorial_Comment {

	const POST_TYPE = 'wp-editorial-comment';

	/**
	 * Get a specific editorial comment.
	 *
	 * @param int $id
	 *
	 * @return WP_Post|null
	 */
	public static function get( $id ) {
		return get_post( $id );
	}

	/**
	 * Get all editorial comments for a specific post.
	 *
	 * @param WP_Post $post
	 *
	 * @return WP_Post[]
	 */
	public static function getAll( WP_Post $post ) {
		$comments = wp_cache_get( $post->ID . '-editorial-comments' );
		if ( ! $comments ) {
			$comments = array();
			$query = new WP_Query( array(
				'post_type'      => self::POST_TYPE,
				'post_status'    => 'publish',
				'post_parent'    => $post->ID,
				'posts_per_page' => 100,
				'order'          => 'ASC',
			) );
			if ( $query->have_posts() ) {
				$comments = $query->posts;
				wp_cache_set( $post->ID . '-editorial-comments', $comments, '', HOUR_IN_SECONDS );
			}
		}

		return $comments;
	}

	/**
	 * Add an editorial comment to a specific post.
	 *
	 * @param WP_Post $post
	 * @param string $comment
	 *
	 * @return int
	 */
	public static function add( WP_Post $post, $comment ) {
		$post_id = wp_insert_post( array(
			'post_content' => force_balance_tags( wp_kses_post( $comment ) ),
			'post_type'    => self::POST_TYPE,
			'post_status'  => 'publish',
			'post_parent'  => $post->ID,
		) );
		wp_cache_delete( $post->ID . '-editorial-comments' );

		return $post_id;
	}

	/**
	 * Edit a specific editorial comment.
	 *
	 * @param int $id
	 * @param string $comment
	 *
	 * @return int
	 */
	public static function edit( $id, $comment ) {
		$post_id = wp_update_post( array(
			'ID'           => $id,
			'post_content' => force_balance_tags( wp_kses_post( $comment ) ),
		) );
		wp_cache_delete( get_post( $id )->post_parent . '-editorial-comments' );

		return $post_id;
	}

	/**
	 * Delete a specific editorial comment.
	 *
	 * @param int $id
	 */
	public static function delete( $id ) {
		wp_cache_delete( get_post( $id )->post_parent . '-editorial-comments' );
		wp_delete_post( $id, true );
	}

	/**
	 * Delete all editorial comments for a post.
	 *
	 * @param WP_Post $post
	 */
	public static function deleteAll( WP_Post $post ) {
		$comments = self::getAll( $post );
		foreach ( $comments as $comment ) {
			self::delete( $comment->ID );
		}
	}

}