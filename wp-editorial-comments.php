<?php

/*
 * Plugin Name: Micah Wood
 * Plugin URI:
 * Description:
 * Version: 0.1.0
 * Author: Micah Wood
 * Author URI:  http://wpscholar.com
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wp-editorial-comments
 */

if ( ! defined( 'WP_EDITORIAL_COMMENTS_DIR' ) ) {
	define( 'WP_EDITORIAL_COMMENTS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'WP_EDITORIAL_COMMENTS_URL' ) ) {
	define( 'WP_EDITORIAL_COMMENTS_URL', plugin_dir_url( __FILE__ ) );
}

require __DIR__ . '/includes/ajax.php';
require __DIR__ . '/includes/post-type.php';
require __DIR__ . '/includes/setup.php';

add_action( 'init', array( 'WP_Editorial_Comments', 'initialize' ) );