jQuery(document).ready(function ($) {

    $('[data-action="wp-editorial-comment-add"]').on('click', onAdd);
    $('[data-action="wp-editorial-comment-edit"]').on('click', onEdit);
    $('[data-action="wp-editorial-comment-save"]').on('click', onSave);
    $('[data-action="wp-editorial-comment-cancel"]').on('click', onCancel);
    $('[data-action="wp-editorial-comment-delete"]').on('click', onDelete);
    $('[data-action="wp-editorial-comment-delete-all"]').on('click', onDeleteAll);

    function onAdd() {
        $.post(
            ajaxurl,
            {
                id: $('[name="post_ID"]').val(),
                comment: $('[name="wp-editorial-comment"]').val(),
                action: $(this).attr('data-action'),
                nonce: $(this).attr('data-nonce')
            },
            function (response) {
                if (response.hasOwnProperty('success') && response.success) {

                    var comment = $(response.data.html);
                    comment.find('[data-action="wp-editorial-comment-edit"]').on('click', onEdit);
                    comment.find('[data-action="wp-editorial-comment-save"]').on('click', onSave);
                    comment.find('[data-action="wp-editorial-comment-cancel"]').on('click', onCancel);
                    comment.find('[data-action="wp-editorial-comment-delete"]').on('click', onDelete);
                    $('.wpec-comment-list').append(comment);

                    $('[name="wp-editorial-comment"]').val('');
                }
            }
        );

        console.log('add');
    }

    function onEdit() {
        var comment = $(this).closest('.wpec-comment');
        var text = comment.find('.wpec-comment-display').hide().find('.wpec-comment-content').html().trim();
        comment.find('.wpec-comment-edit-form').css('display', 'flex').find('textarea').html(text);

        console.log('edit');
    }

    function onSave() {
        var comment = $(this).closest('.wpec-comment');

        $.post(
            ajaxurl,
            {
                id: $(this).attr('data-id'),
                comment: $('#wp-editorial-comment-' + $(this).attr('data-id')).val(),
                action: $(this).attr('data-action'),
                nonce: $(this).attr('data-nonce')
            },
            function (response) {
                if (response.hasOwnProperty('success') && response.success) {
                    comment.find('.wpec-comment-edit-form').hide();
                    comment
                        .find('.wpec-comment-display').css('display', 'flex')
                        .find('.wpec-comment-content').html(response.data.comment);
                }
            }
        );

        console.log('save');
    }

    function onCancel() {
        var comment = $(this).closest('.wpec-comment');
        comment.find('.wpec-comment-display').css('display', 'flex');
        comment.find('.wpec-comment-edit-form').hide();

        console.log('cancel');
    }

    function onDelete() {
        $.post(
            ajaxurl,
            {
                id: $(this).attr('data-id'),
                action: $(this).attr('data-action'),
                nonce: $(this).attr('data-nonce')
            },
            function (response) {
                if (response.hasOwnProperty('success') && response.success) {
                    $(this).closest('.wpec-comment').fadeOut(1000, function () {
                        $(this).remove()
                    });
                }
            }.bind(this),
            'json'
        );

        console.log('delete');
    }

    function onDeleteAll() {
        $.post(
            ajaxurl,
            {
                id: $('[name="post_ID"]').val(),
                action: $(this).attr('data-action'),
                nonce: $(this).attr('data-nonce')
            },
            function (response) {
                if (response.hasOwnProperty('success') && response.success) {
                    $('.wpec-comment').fadeOut(1000, function () {
                        $(this).remove();
                    });
                }
            }
        );

        console.log('delete-all');
    }

});