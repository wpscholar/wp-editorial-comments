<div class="wpec-comment-form">

	<textarea name="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE ); ?>"></textarea>

	<button type="button"
	        class="button"
	        data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-add' ); ?>"
	        data-nonce="<?php echo esc_attr( wp_create_nonce( WP_Editorial_Comment::POST_TYPE . '-add' ) ); ?>">
		<?php esc_html_e( 'Add Comment', 'wp-editorial-comments' ); ?>
	</button>

	<?php if ( current_user_can( 'moderate_comments' ) ): ?>
		<a href="javascript:;"
		   data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-delete-all' ); ?>"
		   data-nonce="<?php echo esc_attr( wp_create_nonce( WP_Editorial_Comment::POST_TYPE . '-delete-all' ) ); ?>">
			<?php esc_html_e( 'Delete All', 'wp-editorial-comments' ); ?>
		</a>
		<?php wp_nonce_field( WP_Editorial_Comment::POST_TYPE . '-delete-all', WP_Editorial_Comment::POST_TYPE . '-delete-all_nonce' ); ?>
	<?php endif; ?>

	<?php wp_nonce_field( WP_Editorial_Comment::POST_TYPE . '-add', WP_Editorial_Comment::POST_TYPE . '-add_nonce' ); ?>

</div>