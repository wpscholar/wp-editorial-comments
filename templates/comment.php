<div class="wpec-comment">

	<div class="wpec-comment-user">
		<?php echo get_avatar( $comment->post_author, 64 ); ?>
		<div><?php the_author(); ?></div>
	</div>

	<div class="wpec-comment-display">

		<div class="wpec-comment-content">
			<?php the_content(); ?>
		</div>

		<?php if ( get_current_user_id() === absint( $comment->post_author ) || current_user_can( 'moderate_comments' ) ): ?>

			<div class="wpec-comment-links">
				<a href="javascript:;"
				   data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-edit' ); ?>">
					<?php esc_html_e( 'Edit', 'wp-editorial-comments' ); ?>
				</a>
				<a href="javascript:;"
				   data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-delete' ); ?>"
				   data-id="<?php echo absint( $comment->ID ); ?>"
				   data-nonce="<?php echo esc_attr( wp_create_nonce( WP_Editorial_Comment::POST_TYPE . '-delete' ) ); ?>">
					<?php esc_html_e( 'Delete', 'wp-editorial-comments' ); ?>
				</a>
			</div>

		<?php endif; ?>

	</div>

	<?php if ( get_current_user_id() === absint( $comment->post_author ) || current_user_can( 'moderate_comments' ) ): ?>
		<div class="wpec-comment-edit-form">
					<textarea id="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-' . $comment->ID ) ?>"></textarea>
			<button type="button"
			        class="button"
			        data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-save' ); ?>"
			        data-id="<?php echo absint( $comment->ID ); ?>"
			        data-nonce="<?php echo esc_attr( wp_create_nonce( WP_Editorial_Comment::POST_TYPE . '-save' ) ); ?>">
				<?php esc_html_e( 'Update', 'wp-editorial-comments' ); ?>
			</button>
			<a href="javascript:;"
			   data-action="<?php echo esc_attr( WP_Editorial_Comment::POST_TYPE . '-cancel' ); ?>">
				<?php esc_html_e( 'Cancel', 'wp-editorial-comments' ); ?>
			</a>
		</div>
	<?php endif; ?>

</div>