<?php
if ( ! isset( $post ) ) {
	$post = get_post();
}
$comments = WP_Editorial_Comment::getAll( $post );
?>

<div class="wpec-comment-list">

	<?php if ( $comments ): ?>

		<?php foreach ( $comments as $comment ): ?>

			<?php setup_postdata( $comment ); ?>

			<?php require __DIR__ . '/comment.php'; ?>

		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>

	<?php endif; ?>

</div>